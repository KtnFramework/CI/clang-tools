FROM opensuse/tumbleweed

RUN zypper \
    --non-interactive install \
    clang
